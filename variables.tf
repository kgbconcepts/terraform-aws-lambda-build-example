variable "function_name" {
  type = string
  description = "the function/app name for directory"
  default = "example"
}

variable "archive_name" {
  type = string
  description = "the output zip file name"
  default = "example"
}

variable "archive_type" {
  type = string
  description = "the archive type, supports zip"
  default = "zip"
}
