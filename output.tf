output "size" {
  value = data.archive_file.function_archive.output_size
}

output "sha" {
  value = data.archive_file.function_archive.output_sha
}

output "base64sha256" {
  value = data.archive_file.function_archive.output_base64sha256
}

output "md5" {
  value = data.archive_file.function_archive.output_md5
}

output "file_path" {
  value = "${path.module}/functions/${var.function_name}/${var.archive_name}.${var.archive_type}"
}
