# meta version 1.0.0
terraform {
  required_version = "~> 0.12.0"

  required_providers {
    aws   = "~> 2.0"
    template = "~> 2.0"
  }
}
