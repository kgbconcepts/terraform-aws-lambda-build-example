data "archive_file" "function_archive" {
  type        = var.archive_type
  source_dir = "${path.module}/functions/${var.function_name}"
  output_path = "${path.module}/functions/${var.function_name}/${var.archive_name}.${var.archive_type}"
}
